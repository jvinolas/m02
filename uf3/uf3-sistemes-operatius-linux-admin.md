#  3 - Administració de Sistemes Operatius Linux

Taula de continguts

[TOC]

## 3.1 - Serveis: SystemD

### 3.1.1 La polèmica amb SysV

Tradicionalment els sistemes Linux han gestionat els seus serveis amb **SysV**. Eren un conjunt d'scripts a ```/etc/init.d``` en distribucions basades en Debian i a ```/etc/rc.d/init.d``` en les basades en RedHat. Podíem crear els nostres propis gestors de serveis *SysV* fent un script com [aquest](https://www.cyberciti.biz/tips/linux-write-sys-v-init-script-to-start-stop-service.html). Executant aquests scripts amb els paràmetres *start*, *stop*, *status*, *restart* i *reload* podíem iniciar, aturar, etc... el servei corresponent. Per exemple el següent script iniciaria el servidor apache:

`/etc/init.d/httpd start`

Per tant el sistema tradicional *SysV* es basava en els principis de simplicitat i de gestionar només l'arrencada i aturada de serveis correctament dins d'uns nivells establerts, els [runlevels](https://learn.adafruit.com/running-programs-automatically-on-your-tiny-computer/sysv-init-runlevels) que el definien l'ordre. 

Un bon dia de 2010 un noi a RedHat va decidir que era millor canviar completament la manera com s'inicien els serveis al linux, principalment per intentar aportar una millor definició de dependències entre serveisi permetre també més execucions en paral.lel i concurrents, especialment durant l'arrencada del sistema operatiu. Aquí teniu a Lennart Poettering en una entrevista:

<iframe src="//commons.wikimedia.org/wiki/File:LCA_TV-_Lennart_Poettering.webm?embedplayer=yes" width="640" height="480" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>

Aviat aquest nous sitema, el **SystemD** va ser adaptat per les distribucions basades en RedHat i va crear grans debats dins la comunitat Linux, especialment a Debian, on inclús van arribar a enemistar-se'n els seus mantenidors arran de la polèmica sobre si abandonar SysV pel SystemD o no. Així que ara existeix la distribució Debian (amb el nou SystemD) i la [Debuan](https://devuan.org/) que segueix resistint-se a abandonar el SysV.

Els defensors del *SysV* es basen en la filosofia original de Linux "fes una cosa i fes-la bé". Aquest nou *SystemD* crea múltiples binaris que miren de fer moltes més coses de les que tradicionalment feina el *SysV*. Això fa que el sistema passi a ser molt més complex ja que ara ja no gestiona l'arrencada i aturada de serveis solsament sinó que ens trobem que fa control d'energia, gestió de dispositius, gestió de logins, particions GPT, ...

### 3.1.2 SystemD

Per a gestionar els serveis ara ho farem amb l'ordre **systemctl**. Aquesta ordre buscarà el fitxer de configuració del servei per tal de realitzar-ne l'acció corresponent que s'hi indiqui. Tots els serveis poden tenir dependències i això farà que no només s'iniciï el nostre servei sinó també els associats.

#### 3.1.2.1 Fitxers de configuració: Service units

Els fitxers de configuració de serveis de systemd tenen extensió *.service* i es troben en tres llocs:

-  /usr/lib/systemd/system: Aquí hi ha els que venen amb els paquets que intal.lem (.deb, .rpm,...)
- /run/systemd/system: Aquí hi ha els que es creen en inicial el sistema. Són més prioritaris que els anterior.
- /etc/systemd/system: Aquí és on com administrador podem afegir-hi les nostres configuracions de serveis, que seran més prioritaris que tots els anteriors.

Podem veure que el systemd està corrent al nostre sistema:

```
ps -eaf | grep [s]ystemd
```

I en realitat veureu que té diversos serveis que està controlant: dispositius (udev), login, sessions... Veureu que n'hi ha un que té el PID 1, el principal. D'aquí deriven tota la resta de processos, que haurà arrencat aquest servei en pendre el control durant l'arrencada. Podem analitzar l'arrencada del nostre sistema:

```
systemd-analyze
```

I inclús veure-ho amb més detall:

```
systemd-analyze blame
```

Podem veure la cadena d'inici de serveis. Veureu que no només hi ha serveis (.service), també hi haurà altres unitats com targets, mounts, sockets, devices... cadascún tindrá una funció específica, no només hi haurà serveis genèrics que podrem iniciar o aturar.

Podem veure totes les unitats disponibles amb:

```
systemctl list-unit-files
```

I les que s'estan executant:

```
systemctl list-units
```

O les que han fallat en iniciar-se:

```
systemctl --failed
```

Per exemple podem comprovar si el servei de cron està funcionant: systemctl is-enabled crond.service o veure-ho amb més detall amb:

```
systemctl status crond.service
```

També podem veure tots els serveis que necessita un servei per tal de ser iniciat amb el temps que han trigat en iniciar-se:

```
systemd-analyze critical-chain httpd.service
```

I veure'n les dependències:

```
systemctl list-dependencies httpd.service
```



Per actuar amb els serveis podem llistar els que hi han disponibles i executar una de les accions que volguem:

```
systemctl list-unit-files --type=service
```

```
systemctl start httpd.service
systemctl restart httpd.service
systemctl stop httpd.service
systemctl reload httpd.service
systemctl status httpd.service
```

I per a fer que un servei s'iniciï sempre durant l'arrencada ho farem amb *enable* o *disable*. 

##### Fitxer de configuració de servei

Imaginem que hem creat un servidor web mitjançant python desde la línia d'ordres:

```python -m SimpleHTTPServer```

Podeu comprovar amb un navegador que si accedir al port per defecte (http://localhost:8000/) hi veureu els fitxers del directori on heu arrencar el servidor.

Ara el que volem és poder controlar aquest servidor mitjançant un servei nostre que crearem. Aquest servei podrà iniciar-se o aturar-se i establir-ne l'inici durant l'arrencada del sistema operatiu.

Podem crear un fitxer a /etc/systemd/system/ anomenat webserver.service

```
[Unit]
Description=El meu servidor web

[Service]
ExecStart=/usr/bin/python -m SimpleHTTPServer

[Install]
WantedBy=multi-user.target
```

Una vegada creat si volem provar si funciona primer caldrà recarregar els serveis:

``` systemctl daemon-reload```

i ara ja tindrem disponible el servei. Prove d'iniciar-lo, aturar-lo, etc... Teniu moltes més [opcions de configuració](https://www.shellhacks.com/systemd-service-file-example/).

Podeu veure els logs del servei amb *journalctl -u webserver*. Més endavant tractarem el tema dels logs.

#### 3.1.2.2 Nivells: Target units

Antigament hi havia els *runlevels* del 0 al 6 al SysV que en passar a SystemD són els *Targets*. Aquests fitxers porten com extensió *.target*. 

La funció dels targets no és una altra que agrupar una sèrie de *service units* amb les seves dependències. Per exemple existeix el *graphical.target* que pot arrencar serveis com el *gdm.service*, *accounts-daemon.service*, etc... i ho farà en ordre i amb les sesves dependències.

Aquí tenim una relació entre els nivells que existien antigament (SysV) i els actuals targets de SystemD:

| Runlevel | Target Units                            | Description                               |
| -------- | --------------------------------------- | ----------------------------------------- |
| `0`      | `runlevel0.target`, `poweroff.target`   | Shut down and power off the system.       |
| `1`      | `runlevel1.target`, `rescue.target`     | Set up a rescue shell.                    |
| `2`      | `runlevel2.target`, `multi-user.target` | Set up a non-graphical multi-user system. |
| `3`      | `runlevel3.target`, `multi-user.target` | Set up a non-graphical multi-user system. |
| `4`      | `runlevel4.target`, `multi-user.target` | Set up a non-graphical multi-user system. |
| `5`      | `runlevel5.target`, `graphical.target`  | Set up a graphical multi-user system.     |
| `6`      | `runlevel6.target`, `reboot.target`     | Shut down and reboot the system.          |

Podem veure el target default que establirà el sistema en arrencar amb:

```
~]$ systemctl get-default
graphical.target
```

LListar tots els targets actius:

```
systemctl list-units --type target
```

Llistar tots els targets disponibles:

```
systemctl list-units --type target --all
```

Llistar tots els fitxers associats amb els targets:

```
systemctl list-unit-files --type target
```

Canviar el default target (que es carregarà en la propera arrencada):

```
systemctl set-default graphical.target
```

També podem canviar-ho en la sessió actual:

```
systemctl isolate graphical.target
```

[https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/system_administrators_guide/sect-managing_services_with_systemd-targets](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/system_administrators_guide/sect-managing_services_with_systemd-targets)

## 3.2 - Logs: Journal

Tradicionalment hem accedit als logs en fitxers de text en format **syslog** a /var/log/ però amb l'arribada del *systemd* també va arribar el **journald**. La comanda per accedir-hi serà el **journalctl**:

```bash
$ journalctl
-- Logs begin at Sat 2019-03-16 12:13:00 CET, end at Sun 2019-04-07 22:15:01 CEST. --
mar 16 12:13:00 pepino.localdomain fctsched[809]: Set send timeout rc:0
...
```

El Journald emmagatzema els logs en format binari. Per accedir-hi farem ús de l'ordre [journalctl](https://www.freedesktop.org/software/systemd/man/journalctl.html). Teniu moltíssimes [opcions per a filtrar els logs](https://www.digitalocean.com/community/tutorials/how-to-use-journalctl-to-view-and-manipulate-systemd-logs) però algunes de les més útils per nosaltres ara són:

Veure logs **per unitats**, per exemple d'un servei concret:

```bash
journalctl -u nginx.service
journalctl -u nginx.service -u mysql.service
```

Gestionar els logs dels **inicis del sistema**:

```journalctl -b
journalctl -b
journalctl --list-boots
journalctl -b -1
```

O veure per **dates**:

```
journalctl --since "2015-01-10 17:15:00"
journalctl --since "1 hour ago"
journalctl --since "2 days ago"
journalctl --since "2018-06-26 23:15:00" --until "2018-06-26 23:20:00"
```

Veure **missatges del kernel**:

```
journalctl -k
journalctl -k -b -5
```

O veure els més importants segons l'**escala de prioritats**:

- 0: emerg
- 1: alert
- 2: crit
- 3: err
- 4: warning
- 5: notice
- 6: info
- 7: debug

```bash
journalctl -p err -b
```

Són interessants les opcions de seguiment (follow) i de mostrar pel final:

```bash
journalctl -f
journalctl -u mysql.service -f
```

I també per a veure pel final un nombre de línies o veure-ho en ordre invers:

```bash
journalctl -n 50 --since "1 hour ago"
journalctl -u sshd.service -r -n 1
```

També tenim possibilitat de mostrar els logs en **formats de sortida** diferents:

```bash
journalctl -u sshd.service -r -n 10 -o json-pretty
```

Alguns dels formats admesos són:

- json
- json-pretty
- verbose
- cat
- short

Podem enviar missatges al journal del sistema per a fer proves nosaltres mateixos amb l'ordre *systemd-cat*:

```bash
echo 'Bon dia' | systemd-cat
```

Si en un altre terminal mirem a la vegada la sortida del journal del nostre sistema ho podrem veure:

```bash
jounralctl -f
```

També podem establir prioritats:

```bash
echo 'Es cala foc!' | systemd-cat -p emerg
```

O indicar-ne el programa que l'ha generat:

```bash
echo 'Es cala foc!' | systemd-cat -t bombers_app -p emerg
```

Podem enviar la sortida d'una comanda al journal posant la comanda al final:

```bash
systemd-cat -t mipc ls
```

## 3.3 - Gestió de xarxes

### Link layer[¶](http://thedocs.isardvdi.com/networking/concepts/#link-layer)

Mostrar les interfícies:

```
ip link show
```

Enable/disable una interfície:

```
ip link set eth0 down
ip link set eth0 up
```

Canviar noms i adreces físiques d'una interfície:

```
ip link set eth0 down
ip link set eth0 address 00:11:22:33:44:55
ip link set eth0 name lan
ip link set eth0 up
```

Mostrar paràmetres físics de l'interfície:

```
ethtool eth0
```

Mostrar la velocitat de l'interfície:

```
ethtool eth0 |grep Speed
```

Mòdul del kernel que usa l'interfície:

```
ethtool -i eth0
```

Mostrar estadístiques de l'interfície

```
ethtool -S eth0
```

### Test de velocitat de xarxa amb iperf[¶](http://thedocs.isardvdi.com/networking/concepts/#test-network-speed-with-iperf)

Traditional tool is **iperf**, with speeds greater than 1Gbps new tool with more options **iperf3** is better

```
#Server
iperf -s 
#Client
iperf -c SERVER_IP
```

Some useful options:

```
#Server
iperf -s -i 2 -p 5002
#Client
iperf -p 5002 -c SERVER_IP
```

-t to increase duration of test

```
#Server
iperf -s -i 2
#Client
iperf -t 30 -c SERVER_IP
```

### Test de velocitat de xarxa amb netperf[¶](http://thedocs.isardvdi.com/networking/concepts/#test-network-speed-with-netperf)

Realistic test with tcp both directions throughput.

- https://github.com/HewlettPackard/netperf

Start netserver on one host and execute script on client:

vi netperf-test.sh

```
 for i in 1
     do
      netperf -H 10.1.2.102 -t TCP_STREAM -B "outbound" -i 10 -P 0 -v 0 \
        -- -s 256K -S 256K &
      netperf -H 10.1.2.102 -t TCP_MAERTS -B "inbound"  -i 10 -P 0 -v 0 \
        -- -s 256K -S 256K &
     done
```

It will run on background and output to console results when it finishes.

You can check live network throughput during test by using iftop:

```
iftop -i eth0
```

### Bridges[¶](http://thedocs.isardvdi.com/networking/concepts/#bridges)

Un pont es crea entre diferents interfícies i fa que es comportin com un únic dispositiu. En realitat és l'equivalent en programari d'un switch:

```
brctl addif br0 usb0
```

Assign interfaces to bridge:

```
brctl addif br0 eth0
brctl addif br0 usb0
brctl addif br0 usb1
brctl delif br0 usb1
```

To show interfaces members of a bridge and table of ports and mac address:

```
brctl show
brctl show br0
brctl showmacs br0
```

## Capture packets[¶](http://thedocs.isardvdi.com/networking/concepts/#capture-packets)

**tcpdump**: herramienta clásica para hacer capturas

```
tcpdump -w 08232010.pcap -i eth0
```

**tshark**: wireshark in command line, capture filters show [https://wiki.wireshark.org/CaptureFilters]

- **[ -i ]** Input interface
- **[ -w ]** Output file
- **[ -r ]** Read options from file
- **[ -c ]** limit packets captured
- **[ -a duration:X]** seconds when stop capture
- **[ -Y ]** Wireshark filters

Examples:

```
tshark -i enp3s0 -w /tmp/out.pcap -c 10
tshark -i enp3s0 -w /tmp/out.pcap -a duration:4
tshark -r /tmp/out.pcap -w /tmp/web.pcap -Y "http"

tshark -i enp3s0 -w /tmp/ping.pcap "host 8.8.8.8"
tshark -i enp3s0 -w /tmp/web.pcap "port 80"
```

To show in screen the packets with extended version (-x)

```
tshark -r /tmp/web.pcap
tshark -r /tmp/web.pcap -x 
tshark -r /tmp/web.pcap -x -Y "frame.number==10"
```

## Network Layer[¶](http://thedocs.isardvdi.com/networking/concepts/#network-layer)

Show ip address, short command of **ip address show**:

```
ip a
```

Show stats (tx,rx,error,dropped)

```
ip -statistics a
```

## Fixed ip address:[¶](http://thedocs.isardvdi.com/networking/concepts/#fixed-ip-address)

Delete all address of an interface (flush):

```
ip a f dev eth0
```

Asociar una ip a una interfaz (no olvidar la máscara):

```
ip a a 192.168.100.10/24 dev eth0
```

Se pueden asociar más de una ip a una interfaz y eliminar una en concreto:

```
ip a a 192.168.100.10/24 dev eth0
ip a a 192.168.200.11/27 dev eth0

ip a d 192.168.200.11/27 dev eth0
```

## Ips dinámicas:[¶](http://thedocs.isardvdi.com/networking/concepts/#ips-dinamicas)

Liberar la ip actual:

```
dhclient -r
```

Esto debería haber liberado la ip actual y el daemon debería haber finalizado. En caso de que no podamos pedir una nueva ip no nos queda otra más que matar ese proceso con:

```
killall dhclient
```

Para pedir una nueva ip en cualquier interface:

```
dhclient
```

Y en una interface en concreto:

```
dhclient eth0
```

Si quieres ver más detalle de la concesión de ip:

```
dhclient -v eth0
dhclient -v -lf /tmp/eth0.lease
cat /tmp/eth0.lease
```

## Tabla ARP[¶](http://thedocs.isardvdi.com/networking/concepts/#tabla-arp)

La orden tradicional arp ha quedado centralizada en la utilidad ip con la opción **ip neigh**

Para ve la tabla de arp actual:

```
ip neigh
arp -a
```

Borrar tabla de arp:

```
ip neigh flush all
```

Añadir entrada arp permanente:

```
ip neigh add 192.168.100.1 lladdr 00:11:22:33:44:55 dev enp3s0
```

Ver sólo las entradas reachable:

```
ip neigh show nud reachable
```

## Routing[¶](http://thedocs.isardvdi.com/networking/concepts/#routing)

Ver las rutas con **ip route show**:

```
ip r 
```

Para eliminar todas las rutas "ip route flush":

```
ip r f all
```

Al añadir una dirección ip a una interfaz, se añade una ruta directa que informa que para ir a la red de esa dirección ip se va directamente a través de la interfaz sin necesidad de pasar por ningún router:

```
$ ip r f all
$ ip a a 172.16.0.10/16 dev eth0
$ ip r s 
172.16.0.10/16  dev eth0 [...]
```

Añadir puerta de enlace por defecto:

```
ip r a default via 192.168.1.1
```

Eliminar puerta de enlace por defecto:

```
ip r d default
```

Añadir ruta estática:

```
ip r a 192.168.200.0/24 via 192.168.100.1
```

Activar bit de forwarding para que trabaje como router:

```
echo 1 > /proc/sys/net/ipv4/ip_forward
```

## Resolución de nombres DNS[¶](http://thedocs.isardvdi.com/networking/concepts/#resolucion-de-nombres-dns)

Se consulta la resolución en el fichero **/etc/hosts**, que contiene una línea para el nombre localhost, se pueden añadir líneas para resolver nombres sin utilizar un servidor dns:

```
$ cat /etc/hosts
127.0.0.1       localhost.localdomain localhost
```

Para resolver con servidores dns linux consulta el contenido del fichero /etc/resolv.conf

```
$ cat /etc/resolv.conf 
[...]
nameserver 192.168.1.1
```

Se consulta la línea que empieza con nameserver seguida de la ip del servidor dns que queremos usar. Si al renovar la ip dinámica el servidor dhcp ofrece un servidor dns se sobreescribe este fichero

Para forzar la resolución de nombres a través de un servidor dns de forma manual:

```
echo "nameserver 8.8.8.8" > /etc/resolv.conf
```

## Pruebas de conectividad[¶](http://thedocs.isardvdi.com/networking/concepts/#pruebas-de-conectividad)

Comprabar respuestas protocolo ARP:

```
arping 192.168.100.1
```

Hacer ping indefinidamente (para terminar ctrl + C)

```
ping 8.8.8.8
```

Limitar número de pings

```
ping -c 2 8.8.8.8
```

**fping**: Hacer pings a varios equipos de una red:

```
fping -g 192.168.100.100 192.168.100.110 -a -q
fping -g 192.168.100.0/24 -a -q
```

**nmap**

Sondeo ping:

```
nmap -sP 192.168.100.0/24
```

Sondeo de puerto 22

## Traceroute y geolocalizar ips[¶](http://thedocs.isardvdi.com/networking/concepts/#traceroute-y-geolocalizar-ips)

**traceroute**: Herramienta histórica para descubrir rutas

```
traceroute 8.8.8.8
```

**mtr**: Si quieres más información, estadísticas y detalle de geolocalización de ips

```
dnf -y install mtr GeoIP GeoIP-GeoLite-data GeoIP-GeoLite-data-extra
mtr -n --report www.gencat.cat
geoiplookup 72.52.92.222
```

# Capa de transporte[¶](http://thedocs.isardvdi.com/networking/concepts/#capa-de-transporte)

## nmap y sondeo de puertos[¶](http://thedocs.isardvdi.com/networking/concepts/#nmap-y-sondeo-de-puertos)

Sondeo de puertos TCP típicos:

```
nmap -sS 192.168.100.1
```

Sondeo de un puerto concreto

```
nmap -sS -p 80 192.168.100.1
```

Sondeo de rango de puertos

```
nmap -sS -p 8080-8090 192.168.100.1
```

Sondeo de puertos aunque el ping no conteste, añadiendo la opción -PN

```
nmap -sS -PN -p 80 192.168.100.1
```

Sondeo de puerto UDP:

```
nmap -sU -p 53 192.168.100.1
```

## netstat[¶](http://thedocs.isardvdi.com/networking/concepts/#netstat)

número de puertos tcp y udp escuchando con el programa asociado:

```
netstat -utlnp
```

Las opciones se pueden combinar sin importar el orden en que van las letras, las típicas de netstat son: *-u => puertos udp* -t => puertos tcp *-p => programa que está usando esa conexión y pid (sólo lo puedes ver si eres root)* -n => muestra el número de puerto *-a => todas las conexiones*-l => puertos en escucha

Todas las conexiones tcp:

```
netstat -tanp 
```

## 3.4 - Tallafocs: FirewallD